import 'dart:ffi';

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import './SplashScreen.dart';

class Flurorouter {
  static final FluroRouter router = FluroRouter();

  static Handler _splashHandler = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          SplashScreen());

  static Handler _mainHandler = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          SplashScreen());

  static Void setupRouter() {
    router.define(
      '/',
      handler: _splashHandler,
    );

    router.define(
      '/main/:name',
      handler: _mainHandler,
      transitionType: TransitionType.fadeIn,
    );
  }
}
