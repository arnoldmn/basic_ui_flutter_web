import 'package:basic_web_flutter/Calendar/calendar_space.dart';
import 'package:basic_web_flutter/Dashboard/dashboard.dart';
import 'package:basic_web_flutter/Navigation/navigation_bar.dart';
import './routes.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    Flurorouter.setupRouter();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            NavigationBar(),
            Dashboard(),
            CalendarSpace(),
          ],
        ),
      ),
    );
  }
}
