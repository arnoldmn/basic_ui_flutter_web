import 'package:flutter/material.dart';

class CompanyName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70.0,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "M",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Colors.amber,
                  fontSize: 20),
            ),
            Text(
              "umbere",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Colors.amber,
                  fontSize: 15),
            )
          ],
        ),
      ),
    );
  }
}
