import 'package:basic_web_flutter/Calendar/src/calendar_section.dart';
import 'package:basic_web_flutter/Calendar/src/meeting_section.dart';
import 'package:basic_web_flutter/Calendar/src/top_container.dart';
import 'package:flutter/material.dart';

class CalendarSpace extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        color: Color(0xfff7f7ff),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width * 0.26,
        child: Column(
          children: [
            SizedBox(
              height: 30.0,
            ),
            TopContainer(),
            CalendarSection(),
            MeetingSection(),
            ClipRRect(
              borderRadius: BorderRadius.circular(
                10.0,
              ),
              child: Image.asset(
                'images/john.jpeg',
                height: 300.0,
                width: 400.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
