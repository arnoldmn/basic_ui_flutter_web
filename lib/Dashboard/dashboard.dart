import 'package:basic_web_flutter/Dashboard/src/files_card.dart';
import 'package:basic_web_flutter/Dashboard/src/project_card.dart';
import 'package:basic_web_flutter/Dashboard/src/project_stats.dart';
import 'package:basic_web_flutter/Dashboard/src/subheader.dart';
import 'package:basic_web_flutter/Dashboard/src/tabs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 100.0,
      child: Container(
        width: MediaQuery.of(context).size.width * 0.68,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(
                left: 30.0,
                top: 25.0,
                bottom: 10.0,
              ),
              child: Text(
                "Ongoing Projects",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
            ),
            Tabs(),
            Container(
              margin: EdgeInsets.only(
                top: 5.0,
              ),
              height: 200.0,
              width: MediaQuery.of(context).size.width * 0.65,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ProjectCard(
                    color: Color(0xffFF4C60),
                    projectName: 'Black Panther',
                    progressIndicatorColor: Colors.redAccent,
                    icon: Feather.moon,
                    percentComplete: "34",
                  ),
                  ProjectCard(
                    color: Color(0xff6C6CE5),
                    projectName: 'Amarghedon',
                    progressIndicatorColor: Colors.redAccent,
                    icon: Feather.truck,
                    percentComplete: "75",
                  ),
                  ProjectCard(
                    color: Color(0xfffAAA1E),
                    projectName: 'Matrix Reloaded',
                    progressIndicatorColor: Colors.redAccent,
                    icon: Icons.local_airport,
                    percentComplete: "80",
                  ),
                ],
              ),
            ),
            SubHeader(
              title: "Shared Files",
            ),
            SharedFilesItem(
              color: Colors.blue,
              fileName: 'Morio TechnoLinks',
              members: '10 members',
              et: '10 Dec 2016',
              fileSize: '2.7 MBs',
            ),
            SharedFilesItem(
              color: Colors.amber,
              fileName: 'Morio TechnoLinks',
              members: '10 members',
              et: '10 Dec 2017',
              fileSize: '8.7 MBs',
            ),
            SharedFilesItem(
              color: Colors.amberAccent,
              fileName: 'Morio TechnoLinks',
              members: '10 members',
              et: '10 Nov 2017',
              fileSize: '5.7 MBs',
            ),
            SubHeader(
              title: "Project Statistics",
            ),
            ProjectStats()
          ],
        ),
      ),
    );
  }
}
